import React, {Component} from 'react';
import Nav from "./Nav"
import {NavLink} from "react-router-dom";

class ContactInfo extends Component {
    render() {
        return (
            <div className="contact-page">
                <Nav></Nav>
                <h3 className="contact-h3">We can be better? Tell us how!</h3>
                <div className="contact-form">
                    <input className="customer-name" type="text" placeholder="Type your name here"/>
                    <textarea className="feedback" cols="30" rows="10"></textarea>
                    <button className="contact-us"><NavLink to="/thankyou">Send</NavLink></button>
                </div>
            </div>
        );
    }
}

export default ContactInfo;