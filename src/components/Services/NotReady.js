import React, {Component} from 'react';

class NotReady extends Component {
    render() {
        return (
            <div>
                <h2>Sorry,this page is not ready yet. Check it later</h2>
            </div>
        );
    }
}

export default NotReady;